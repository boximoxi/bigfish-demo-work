# BigFish Demo work

### Includes

- Bootstrap 4 (SCSS, JS)
- jQuery 
- Popper
- Google Material Icons

### Installation

```
npm install
```

- '`index.html`' as home page
- JavaScripts: `src/js/app.js`
- SCSS: `src/scss/app.scss`
```
npm run build
``` 

### Tasks
- Build sources - ```npm run build```
- Start file watcher for recompiling - ```npm run watch```
- Start webpack dev server - ```npm run start```
- Build sources for production (**with optimization**) - ```npm run production```
- Clean '`dist`' folder - ```npm run clear```
