import 'bootstrap';

$(document).ready(function () {
	const body = $('body');
	const html = $('html');
	const menuButton = $('.btn-mobile-menu');
	const overlay = '.mobile-menu-overlay';

	menuButton.on('click', function() {
		let scrollWidth = window.innerWidth - $(document).width();
		html.toggleClass('no-scroll');
		if (html.hasClass('no-scroll')) {
			body.toggleClass('mobile-menu').css('margin-right', scrollWidth);
			menuButton.toggleClass('is-active');
			body.append('<div class="mobile-menu-overlay"></div>');
		} else {
			body.toggleClass('mobile-menu').css('margin-right', 0);
			menuButton.toggleClass('is-active');
			$(overlay).remove();
		}
	});

	$(document).on('click', overlay, function() {
		$(overlay).remove();
		body.removeClass('mobile-menu');
		menuButton.removeClass('is-active');
		html.toggleClass('no-scroll');
	});

	$('.btn-login').click( function(e) {
		e.preventDefault();
		$('body').toggleClass('loggedin');
	});

});
